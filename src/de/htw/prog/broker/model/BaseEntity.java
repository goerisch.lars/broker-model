package de.htw.prog.broker.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.Table;
import javax.persistence.InheritanceType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlSeeAlso({ Person.class, Auction.class, Bid.class })
@Entity
@Table(schema = "broker", name = "BaseEntity")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "discriminator", discriminatorType = DiscriminatorType.STRING)
public abstract class BaseEntity implements Comparable<BaseEntity> {
	
	@XmlID
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private final long identity;
	
	@XmlElement
	@Column(nullable = false, updatable = false)
	private int version;
	
	@XmlElement
	@Column(nullable = false, updatable = false)
	private final long creationTimestamp;
	
	public BaseEntity() {
		super();
		this.identity = 0;
		this.version = 1;
		this.creationTimestamp = System.currentTimeMillis();
	}

	public int compareTo(BaseEntity b) {
		return Long.compare(this.identity, b.getIdentity());
	}

	public long getIdentity() {
		return identity;
	}


	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public long getCreationTimestamp() {
		return creationTimestamp;
	}
}
