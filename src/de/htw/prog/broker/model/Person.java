package de.htw.prog.broker.model;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(schema = "broker", name = "Person")
@PrimaryKeyJoinColumn(name = "personIdentity")
public class Person extends BaseEntity {
	
	@XmlElement
	@Column(nullable = false, updatable = false, length = 16, unique = true)
	@Size (min=1, max=16)
	private String alias;
	
	@Column(nullable = false, updatable = true, length = 32)
	@Size (min=32, max=32)
	private byte[] passwordHash;
	
	@XmlElement
	@Column(name = "GroupAlias", nullable = false, updatable = true)
	@Enumerated(EnumType.STRING)
	@NotNull
	private Group group;
	
	@XmlElement
	@Embedded
	@NotNull
	@Valid
	private final Name name;
	
	@XmlElement
	@Embedded
	@NotNull
	@Valid
	private final Address address;
	
	@XmlElement
	@Embedded
	@NotNull
	@Valid
	private final Contact contact;
	
	@OneToMany(mappedBy = "seller", cascade = CascadeType.REMOVE)
	private final Set<Auction> auctions;
	
	@OneToMany(mappedBy = "bidder", cascade = CascadeType.REMOVE)
	private final Set<Bid> bids;
	
	public static enum Group {
		ADMIN, USER
	}
	
	static public byte[] passwordHash(String password) {
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
			
			
			return messageDigest.digest(password.getBytes());
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Person() {
		this.bids = new HashSet<Bid>();
		this.auctions  = new HashSet<Auction>();
		this.name = new Name();
		this.address = new Address();
		this.contact = new Contact();
	}
	
	public Person(String alias, byte[] passwordHash, Name name,
			Address address, Contact contact) {
		this();
		this.alias = alias;
		this.passwordHash = passwordHash;
		this.group = Group.USER;
	}

	

	public String getAlias() {
		return alias;
	}

	public byte[] getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(byte[] passwordHash) {
		this.passwordHash = passwordHash;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public Name getName() {
		return name;
	}


	public Address getAddress() {
		return address;
	}


	public Contact getContact() {
		return contact;
	}


	public Set<Auction> getAuctions() {
		return this.auctions;
	}

	
	public Set<Bid> getBids() {
		return this.bids;
	}
	
	public Bid getBid(Auction auction) {
		for (Bid bid : this.bids) {
			if (bid.getAuction().getIdentity() == auction.getIdentity()) {
				return bid;
			}
		}
		return null;
	}
	
}
