package de.htw.prog.broker.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Embeddable
public class Contact {
	
	@Column(nullable = false, updatable = true, length = 63, unique = true)
	//http://www.mkyong.com/regular-expressions/how-to-validate-email-address-with-regular-expression/
	@Pattern(regexp="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$") //nur auf @
	@NotNull
	@Size (min = 1, max = 63)
	private String email;
	
	@Column(nullable = true, updatable = true, length = 63)
	@Size (min = 0, max = 63)
	private String phone;
	
	public Contact() {
		
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
}
