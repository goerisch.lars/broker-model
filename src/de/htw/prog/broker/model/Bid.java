package de.htw.prog.broker.model;

import javax.enterprise.util.AnnotationLiteral;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.glassfish.jersey.message.filtering.EntityFiltering;

import de.sb.java.validation.Inequal;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@XmlRootElement
@Entity
@Table(schema = "broker", name = "Bid", uniqueConstraints = @UniqueConstraint(columnNames = { "bidderReference", "auctionReference" }))
@PrimaryKeyJoinColumn(name = "bidIdentity")
@Inequal(leftAccessPath = "price", rightAccessPath = { "auction", "askingPrice" } , operator = Inequal.Operator.GREATER_EQUAL )
@Inequal(leftAccessPath = { "bidder" , "identity" }, rightAccessPath = { "auction", "seller" , "identity" } , operator = Inequal.Operator.NOT_EQUAL)
public class Bid extends BaseEntity {
	
	/**
	 * Filter annotation for associated bidders marshaled as entities.
	 */
	@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
	@Retention(RetentionPolicy.RUNTIME)
	@EntityFiltering
	@SuppressWarnings("all")
	static public @interface XmlBidderAsEntityFilter {
		static final class Literal extends AnnotationLiteral<XmlBidderAsEntityFilter> implements XmlBidderAsEntityFilter {}
	}

	/**
	 * Filter annotation for associated bidders marshaled as references.
	 */
	@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
	@Retention(RetentionPolicy.RUNTIME)
	@EntityFiltering
	@SuppressWarnings("all")
	static public @interface XmlBidderAsReferenceFilter {
		static final class Literal extends AnnotationLiteral<XmlBidderAsReferenceFilter> implements XmlBidderAsReferenceFilter {};
	}

	/**
	 * Filter annotation for associated auctions marshaled as entities.
	 */
	@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
	@Retention(RetentionPolicy.RUNTIME)
	@EntityFiltering
	@SuppressWarnings("all")
	static public @interface XmlAuctionAsEntityFilter {
		static final class Literal extends AnnotationLiteral<XmlAuctionAsEntityFilter> implements XmlAuctionAsEntityFilter {}
	}

	/**
	 * Filter annotation for associated auctions marshaled as references.
	 */
	@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
	@Retention(RetentionPolicy.RUNTIME)
	@EntityFiltering
	@SuppressWarnings("all")
	static public @interface XmlAuctionAsReferenceFilter {
		static final class Literal extends AnnotationLiteral<XmlAuctionAsReferenceFilter> implements XmlAuctionAsReferenceFilter {}
	}
	
	@XmlElement
	@Column(name ="price", nullable = false, updatable = true)
	@NotNull
	private long price;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "auctionReference", nullable = false, updatable = false)
	@NotNull
	private final Auction auction;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "bidderReference", nullable = false, updatable = false)
	@NotNull
	private final Person bidder;
	
	protected Bid() {
		this(null, null);
	}
	

	public Bid(Auction auction, Person bidder) {
		this.price = auction == null ? 0 : auction.getAskingPrice();
		this.auction = auction;
		this.bidder = bidder;
	}


	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price =  price;
	}
	
	//@XmlElement
	@XmlAuctionAsEntityFilter
	public Auction getAuction() {
		return auction;
	}
	
	@XmlElement
	@XmlAuctionAsReferenceFilter
	public long getAuctionReference() {
		return this.auction == null ? 0 : this.auction.getIdentity();
	}


	@XmlElement
	@XmlBidderAsEntityFilter
	public Person getBidder() {
		return bidder;
	}
	
	@XmlElement
	@XmlBidderAsReferenceFilter
	public long getBidderReference() {
		return this.bidder == null ? 0 : this.bidder.getIdentity();
	}
}
