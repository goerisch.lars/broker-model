package de.htw.prog.broker.model;

import java.util.HashSet;
import java.util.Set;

import javax.enterprise.util.AnnotationLiteral;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.glassfish.jersey.message.filtering.EntityFiltering;

import de.sb.java.validation.Inequal;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@XmlRootElement
@Entity
@Table(schema = "broker", name = "Auction", indexes = @Index(columnList = "closureTimestamp", unique = true))
@PrimaryKeyJoinColumn(name = "auctionIdentity")
@Inequal(leftAccessPath = { "closureTimestamp" }, rightAccessPath = { "creationTimestamp" })
public class Auction extends BaseEntity {
	
	/**
	 * Filter annotation for associated sellers marshaled as entities.
	 */
	@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
	@Retention(RetentionPolicy.RUNTIME)
	@EntityFiltering
	@SuppressWarnings("all")
	static public @interface XmlSellerAsEntityFilter {
		static final class Literal extends AnnotationLiteral<XmlSellerAsEntityFilter> implements XmlSellerAsEntityFilter {}
	}

	/**
	 * Filter annotation for associated sellers marshaled as references.
	 */
	@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
	@Retention(RetentionPolicy.RUNTIME)
	@EntityFiltering
	@SuppressWarnings("all")
	static public @interface XmlSellerAsReferenceFilter {
		static final class Literal extends AnnotationLiteral<XmlSellerAsReferenceFilter> implements XmlSellerAsReferenceFilter {}
	}

	/**
	 * Filter annotation for associated bids marshaled as entities.
	 */
	@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
	@Retention(RetentionPolicy.RUNTIME)
	@EntityFiltering
	@SuppressWarnings("all")
	static public @interface XmlBidsAsEntityFilter {
		static final class Literal extends AnnotationLiteral<XmlBidsAsEntityFilter> implements XmlBidsAsEntityFilter {}
	}



	@XmlElement
	@Column(nullable = false, updatable = true, length = 255)
	@NotNull
	@Size(min = 1, max = 255)
	private String title;

	@XmlElement
	@Column(nullable = false, updatable = true)
	@NotNull
	@Min(value = 1)
	private short unitCount;

	@XmlElement
	@Column(nullable = false, updatable = true)
	@NotNull
	@Min(value = 0)
	private long askingPrice;

	@XmlElement
	@Column(nullable = false, updatable = true)
	@NotNull
	private long closureTimestamp;

	@XmlElement
	@Column(nullable = false, updatable = true, length = 8189)
	@NotNull
	@Size(min = 1, max = 8189)
	private String description;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "sellerReference", nullable = false, updatable = false)
	@NotNull
	private final Person seller;

	@OneToMany(mappedBy = "auction")
	private final Set<Bid> bids;

	protected Auction() {
		this(null);
	}

	public Auction(Person seller) {
		super();
		this.unitCount = 0;
		this.askingPrice = 0;
		this.closureTimestamp = System.currentTimeMillis() + 7 * 24 * 60 * 60 * 1000l;
		this.seller = seller;
		this.bids = new HashSet<Bid>();
	}

	@XmlElement
	public boolean isClosed() {
		return System.currentTimeMillis() > this.closureTimestamp; 
	}

	@XmlElement
	public boolean isSealed() {
		return !this.isClosed() || !this.bids.isEmpty();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public short getUnitCount() {
		return unitCount;
	}

	public long getAskingPrice() {
		return askingPrice;
	}

	public long getClosureTimestamp() {
		return closureTimestamp;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@XmlElement
	@XmlSellerAsEntityFilter
	public Person getSeller() {
		return seller;
	}

	@XmlElement
	@XmlSellerAsReferenceFilter
	public long getSellerReference() {
		return this.seller == null ? 0 : this.seller.getIdentity();
	}

	@XmlElement
	@XmlBidsAsEntityFilter
	public Set<Bid> getBids() {
		return this.bids;
	}

	public Bid getBid(Person bidder) {
		for (Bid bid : this.bids) {
			if (bid.getBidder().getIdentity() == bidder.getIdentity()) {
				return bid;
			}
		}
		return null;
	}

	public void setUnitCount(short unitCount) {
		this.unitCount = unitCount;
	}

	public void setAskingPrice(long askingPrice) {
		this.askingPrice = askingPrice;
	}

	public void setClosureTimestamp(long closureTimestamp) {
		this.closureTimestamp = closureTimestamp;
	}
}
