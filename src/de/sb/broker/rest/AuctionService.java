package de.sb.broker.rest;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.Cache;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Parameter;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.Consumes;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.htw.prog.broker.model.Auction;
import de.htw.prog.broker.model.Bid;
import de.htw.prog.broker.model.Bid.XmlBidderAsEntityFilter;
import de.htw.prog.broker.model.Person;

@Path("/auctions")
public class AuctionService {

	static public LifeCycleProvider lifeCycleProvider = new LifeCycleProvider();

	// GET /auctions
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Auction.XmlSellerAsEntityFilter
	public Response getAuctions(@HeaderParam("Authorization") final String authentication, @QueryParam("closed") final Boolean closed, @QueryParam("title") final String title, @QueryParam("lowerUnitCount") final short lowerUnitCount, @QueryParam("upperUnitCount") final short upperUnitCount, @QueryParam("lowerAskingPrice") final long lowerAskingPrice, @QueryParam("upperAskingPrice") final long upperAskingPrice, @QueryParam("sellerId") final long sellerId, @QueryParam("lowerCreationTimestamp") final Long lowerClosureTimestamp, @QueryParam("upperCreationTimestamp") final Long upperClosureTimestamp) {

		EntityManager brokerManager = LifeCycleProvider.brokerManager();

		try {

			LifeCycleProvider.authenticate(authentication);

			CriteriaBuilder builder = brokerManager.getCriteriaBuilder();
			CriteriaQuery<Object> querybuilder = builder.createQuery();

			Root<Auction> a = querybuilder.from(Auction.class);

			List<Predicate> predicates = new ArrayList<Predicate>();

			if (lowerUnitCount == 0 && upperUnitCount == 0) {
				predicates.add(builder.isNotNull(a.get("unitCount")));
			} else {
				if (lowerUnitCount != 0) {
					predicates.add(builder.greaterThanOrEqualTo(a.get("unitCount"), lowerUnitCount));
				}
				if (upperUnitCount != 0) {
					predicates.add(builder.lessThanOrEqualTo(a.get("unitCount"), upperUnitCount));
				}
			}

			if (lowerAskingPrice == 0 && upperAskingPrice == 0) {
				predicates.add(builder.isNotNull(a.get("unitCount")));
			} else {
				if (lowerAskingPrice != 0) {
					predicates.add(builder.greaterThanOrEqualTo(a.get("askingPrice"), lowerAskingPrice));
				}
				if (upperAskingPrice != 0) {
					predicates.add(builder.lessThanOrEqualTo(a.get("askingPrice"), upperAskingPrice));
				}
			}

			if (lowerClosureTimestamp == null && upperClosureTimestamp == null) {
				predicates.add(builder.isNotNull(a.get("closureTimestamp")));
			} else {
				if (lowerClosureTimestamp != null) {
					predicates.add(builder.greaterThanOrEqualTo(a.get("closureTimestamp"), lowerClosureTimestamp));
				}
				if (upperClosureTimestamp != null) {
					predicates.add(builder.lessThanOrEqualTo(a.get("closureTimestamp"), upperClosureTimestamp));
				}
			}

			if (sellerId == 0) {
				predicates.add(builder.isNotNull(a.get("seller")));
			} else {
				predicates.add(builder.equal(a.get("seller"), brokerManager.find(Person.class, sellerId)));
			}
			if (title == null) {
				predicates.add(builder.isNotNull(a.get("title")));
			} else {
				predicates.add(builder.like(a.get("title"), title));
			}

			querybuilder.where(predicates.toArray(new Predicate[] {}));

			Query query = brokerManager.createQuery(querybuilder);
			Collection<Auction> auctions = query.getResultList();

			
			Collection<Annotation> filterAnnotations = new ArrayList<>();

			if (closed == Boolean.TRUE) {
				for (Iterator<Auction> iterator = auctions.iterator(); iterator.hasNext();) {
					Auction temp = iterator.next();
					if (!temp.isClosed()) iterator.remove();
				}
				filterAnnotations.add(new Auction.XmlBidsAsEntityFilter.Literal());
				filterAnnotations.add(new Bid.XmlBidderAsEntityFilter.Literal());				
			} else if (closed == Boolean.FALSE) {
				for (Iterator<Auction> iterator = auctions.iterator(); iterator.hasNext();) {
					Auction temp = iterator.next();
					if (temp.isClosed()) iterator.remove();
				}
			}
			GenericEntity<?> wrapper = new GenericEntity<Collection<Auction>>(auctions) {
			};

			return Response.ok().entity(wrapper, filterAnnotations.toArray(new Annotation[0])).build();
		} catch (BadRequestException exception) {
			throw new ClientErrorException(400);
		} finally {
			// brokerManager.close();
		}
	}

	// PUT /auctions
	@PUT
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public long addAuction(@HeaderParam("Authorization") final String authentication, final Auction template) {

		EntityManager brokerManager = LifeCycleProvider.brokerManager();

		brokerManager.getTransaction().begin();

		try {

			Person requester = LifeCycleProvider.authenticate(authentication);
			final Auction newAuction;
			if (template.getIdentity() == 0) {
				newAuction = new Auction(requester);
			} else {
				newAuction = brokerManager.find(Auction.class, template.getIdentity());
				if (newAuction == null) throw new ClientErrorException(404);
				if (!requester.equals(newAuction.getSeller())) throw new ClientErrorException(403);
			}

			try {
				newAuction.setClosureTimestamp(template.getClosureTimestamp());
				newAuction.setDescription(template.getDescription());
				newAuction.setTitle(template.getTitle());
				newAuction.setUnitCount(template.getUnitCount());
				newAuction.setAskingPrice(template.getAskingPrice());
				if (newAuction.getIdentity() == 0) brokerManager.persist(newAuction);

			} catch (final NullPointerException | IllegalArgumentException exception) {
				throw new ClientErrorException(400);
			} catch (final IllegalStateException exception) {
				throw new ClientErrorException(409);
			}
			brokerManager.getTransaction().commit();
			Cache cache = brokerManager.getEntityManagerFactory().getCache();
			cache.evict(Auction.class, newAuction.getIdentity());
			return newAuction.getIdentity();
			
		} finally {
			brokerManager.getTransaction().begin();
		}

	}

	// GET /auctions/{identity}
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Path("{identity}")
	@Auction.XmlSellerAsReferenceFilter
	public Auction getAuction(@HeaderParam("Authorization") final String authentication, @PathParam("identity") final long identity) {
		EntityManager brokerManager = LifeCycleProvider.brokerManager();
		try {
			LifeCycleProvider.authenticate(authentication);
			Auction auction = brokerManager.find(Auction.class, identity);
			Set<Bid> da = auction.getBids();
			if (auction == null) throw new ClientErrorException(404);			
			return auction;
		} catch (BadRequestException exception) {
			throw new ClientErrorException(400);
		} finally {
			// brokerManager.close();
		}
	}

		// GET /auctions/{identity}/bid
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Path("{identity}/bid")
	@Bid.XmlBidderAsReferenceFilter
	@Bid.XmlAuctionAsReferenceFilter
	public Bid getAuctionBid(@HeaderParam("Authorization") final String authentication, @PathParam("identity") final long identity) {
		EntityManager entityManager = LifeCycleProvider.brokerManager();

		try {
			final Person requester = LifeCycleProvider.authenticate(authentication);
			final Auction auction = entityManager.find(Auction.class, identity);		

			for (final Bid bid : auction.getBids()) {
				if (bid.getBidder().getIdentity() == requester.getIdentity()) {
					return bid;
				}
			}
	
		} catch (BadRequestException exception) {
			throw new ClientErrorException(400);
		} finally {
			// entityManager.close();
		}
	}

	@PUT
	@Path("{identity}/bid")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public void addAuctionBid(@HeaderParam("Authorization") final String authentication, @PathParam("identity") final long identity, @PathParam("price") final long price) {
		EntityManager entityManager = LifeCycleProvider.brokerManager();
		try {
			final Person requester = LifeCycleProvider.authenticate(authentication);
			final Auction auction = entityManager.find(Auction.class, identity);

			if (auction != null) {
				for (final Bid bid : auction.getBids()) {
					if (bid.getBidder().getIdentity() == requester.getIdentity()) {
						if (price == 0) {
							auction.getBids().remove(bid);
							entityManager.getTransaction().begin();
							entityManager.persist(auction);
							entityManager.getTransaction().commit();
							return;
						} else {
							bid.setPrice(price);
							entityManager.getTransaction().begin();
							entityManager.persist(bid);
							entityManager.getTransaction().commit();
							return;
						}

					}
				}
				Bid bid = auction.getBid(requester);
				bid.setPrice(price);
				entityManager.getTransaction().begin();
				entityManager.persist(bid);
				entityManager.getTransaction().commit();

				
			}
			throw new ClientErrorException(404);	
		} catch (BadRequestException exception) {
			throw new ClientErrorException(400);
		} finally {
			entityManager.getTransaction().begin();
			// entityManager.close();
		}
	}
}
