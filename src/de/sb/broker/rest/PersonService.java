package de.sb.broker.rest;

import java.lang.annotation.Annotation;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Cache;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlElement;
import javax.ws.rs.core.Response;

import de.htw.prog.broker.model.Auction;
import de.htw.prog.broker.model.Bid;
import de.htw.prog.broker.model.Person;

@Path("/people")
public class PersonService {

	private final LifeCycleProvider lifeCycleProvider = new LifeCycleProvider();
	static private final String CRITERIA_QUERY_JPQL = "select p from Person as p where " 
	+ "(:lowerCreationTimestamp is null or p.creationTimestamp >= :lowerCreationTimestamp) and " 
	+ "(:upperCreationTimestamp is null or p.creationTimestamp <= :upperCreationTimestamp) and " 
	+ "(:alias is null or p.alias = :alias) and " + "(:givenName is null or p.name.given = :givenName) and " 
	+ "(:familyName is null or p.name.family = :familyName) and " 
	+ "(:street is null or p.address.street = :street) and " 
	+ "(:postCode is null or p.address.postCode = :postCode) and " 
	+ "(:city is null or p.address.city = :city) and " 
	+ "(:email is null or p.contact.email = :email) and " 
	+ "(:phone is null or p.contact.phone = :phone)";

	public PersonService() {
	}

	// GET /people
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Person[] getPeople(
			@HeaderParam("Authorization") final String authentication, 
			@QueryParam("lowerCreationTimestamp") final Long lowerCreationTimestamp, 
			@QueryParam("upperCreationTimestamp") final Long upperCreationTimestamp, 
			@QueryParam("alias") final String alias, 
			@QueryParam("givenName") final String givenName, 
			@QueryParam("familyName") final String familyName, 
			@QueryParam("street") final String street, 
			@QueryParam("postCode") final String postCode, 
			@QueryParam("city") final String city, 
			@QueryParam("email") final String email, 
			@QueryParam("phone") final String phone) {

		EntityManager brokerManager = LifeCycleProvider.brokerManager();

		try {
			
			LifeCycleProvider.authenticate(authentication);
			
			final TypedQuery<Person> query = brokerManager.createQuery(CRITERIA_QUERY_JPQL, Person.class);
			query.setParameter("lowerCreationTimestamp", lowerCreationTimestamp);
			query.setParameter("upperCreationTimestamp", upperCreationTimestamp);
			query.setParameter("alias", alias);
			query.setParameter("givenName", givenName);
			query.setParameter("familyName", familyName);
			query.setParameter("street", street);
			query.setParameter("postCode", postCode);
			query.setParameter("city", city);
			query.setParameter("email", email);
			query.setParameter("phone", phone);

			final Person[] people = query.getResultList().toArray(new Person[0]);

			return people;
		} catch (BadRequestException exception) {
			throw new ClientErrorException(400);
		} finally {
			
		}
	}

	// PUT /people
	@PUT
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public long addPerson(@HeaderParam("Authorization") final String authentication, @HeaderParam("Set-password") final String password, final Person template) {
		
		EntityManager entityManager = LifeCycleProvider.brokerManager();

		entityManager.getTransaction().begin();
		try {

			final Person requester = LifeCycleProvider.authenticate(authentication);
			if (requester.getGroup() != Person.Group.ADMIN && requester.getIdentity() != template.getIdentity()) {
				throw new ClientErrorException(403);
			}
			
			final Person person;
			if (template.getIdentity() == 0) {
				person = new Person(template.getAlias(), template.getPasswordHash(), template.getName(), template.getAddress(), template.getContact());
			} else {
				person = entityManager.find(Person.class, template.getIdentity());
			}

			if (password != null && !password.isEmpty()) {
				person.setPasswordHash(Person.passwordHash(password));
			}
			if (requester.getGroup() == Person.Group.ADMIN) person.setGroup(template.getGroup());
			String temp = template.getName().getGiven();
			if (temp != null) person.getName().setGiven(temp);
			temp = template.getName().getFamily();
			if (temp != null) person.getName().setFamily(temp);
			temp = template.getAddress().getStreet();
			if (temp != null) person.getAddress().setStreet(temp);
			temp = template.getAddress().getPostCode();
			if (temp != null) person.getAddress().setPostCode(temp);
			temp = template.getAddress().getCity();
			if (temp != null) person.getAddress().setCity(temp);
			temp = template.getContact().getEmail();
			if (temp != null) person.getContact().setEmail(temp);
			temp = template.getContact().getPhone();
			if (temp != null) person.getContact().setPhone(temp);
			if (person.getIdentity() == 0) entityManager.persist(person);

			entityManager.getTransaction().commit();
			Cache cache = entityManager.getEntityManagerFactory().getCache();
			cache.evict(Person.class, person.getIdentity());
			
			return person.getIdentity();
		} catch (BadRequestException exception) {
			throw new ClientErrorException(400);
		}finally {
			entityManager.getTransaction().begin();
//			entityManager.getTransaction().rollback();
//			entityManager.close();
		}
	}

	// GET /people/{identity}
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Path("{identity}")
	public Person getPerson(@HeaderParam("Authorization") final String authentication, @PathParam("identity") final long identity) {
		EntityManager entityManager = LifeCycleProvider.brokerManager();

		try {
			LifeCycleProvider.authenticate(authentication);
			final Person person = entityManager.find(Person.class, identity);
			if (person == null) throw new ClientErrorException(404);
			return person;	
		} catch (BadRequestException exception) {
			throw new ClientErrorException(400);
		} finally {
			// entityManager.close();
		}

	}

	// GET /people/{identity}/auctions
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Path("{identity}/auctions")
	public Response getPersonAuctions(@HeaderParam("Authorization") final String authentication, @PathParam("identity") final long identity, @QueryParam("seller") final Boolean seller, @QueryParam("closed") final Boolean closed) {
		EntityManager entityManager = LifeCycleProvider.brokerManager();
	
		try {
			LifeCycleProvider.authenticate(authentication);			
			final Person person = entityManager.find(Person.class, identity);
			if (person == null) throw new ClientErrorException(404);
			Collection<Auction> auctions = person.getAuctions();
			
			Collection<Annotation> filterAnnotations = new ArrayList<>();
			
			if (closed == Boolean.TRUE) {
				for (Iterator<Auction> iterator = auctions.iterator(); iterator.hasNext();) {
					Auction temp = iterator.next();
					if (!temp.isClosed()) iterator.remove();
				}				
				filterAnnotations.add(new Bid.XmlBidderAsEntityFilter.Literal());
				filterAnnotations.add(new Auction.XmlBidsAsEntityFilter.Literal());
			} else if (closed == Boolean.FALSE) {
				for (Iterator<Auction> iterator = auctions.iterator(); iterator.hasNext();) {
					Auction temp = iterator.next();
					if (temp.isClosed()) iterator.remove();
				}
			}
			
			if (seller == Boolean.TRUE) {
				for (Iterator<Auction> iterator = auctions.iterator(); iterator.hasNext();) {
					Auction temp = iterator.next();
					if (temp.getSeller() != person) iterator.remove();
				}
				filterAnnotations.add(new Auction.XmlSellerAsReferenceFilter.Literal());
			} else if (seller == Boolean.FALSE) {
				for (Iterator<Auction> iterator = auctions.iterator(); iterator.hasNext();) {
					Auction temp = iterator.next();
					if (temp.getSeller() == person) iterator.remove();
				}
				filterAnnotations.add(new Bid.XmlBidderAsEntityFilter.Literal());
			}
			
			final GenericEntity<?> wrapper = new GenericEntity<Collection<Auction>>(auctions){};
		
			return Response.ok().entity(wrapper, filterAnnotations.toArray(new Annotation[0])).build();
		} catch (BadRequestException exception) {
			throw new ClientErrorException(400);
		} finally {
			// entityManager.close();
		}
		
	}

	// GET /people/{identity}/bids
	@GET
	@XmlElement
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Path("{identity}/bids")
	@Bid.XmlBidderAsReferenceFilter
	@Bid.XmlAuctionAsReferenceFilter
	public Set<Bid> getPersonBids(@HeaderParam("Authorization") final String authentication, @PathParam("identity") final long identity) {
		EntityManager entityManager = LifeCycleProvider.brokerManager();

		try {
			final Person requester = LifeCycleProvider.authenticate(authentication);
			final Person person = entityManager.find(Person.class, identity);
			if (person == null) throw new ClientErrorException(404);
			final Set<Bid> bids = new HashSet<Bid>();
			for (final Bid bid : person.getBids()) {
				if (requester.equals(person) || bid.getAuction().isClosed()) bids.add(bid);
			}
			return bids;
		} catch (BadRequestException exception) {
			throw new ClientErrorException(400);
		} finally {
			// entityManager.close();
		}
	}
	
	// Get /people/requester
	@GET
	@Path("requester")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Person queryRequester (@HeaderParam("Authorization") final String authentication) {
		final EntityManager entityManager = LifeCycleProvider.brokerManager();

		try {
			return LifeCycleProvider.authenticate(authentication);
		} catch (BadRequestException exception) {
			throw new ClientErrorException(400);
		} finally {
			// entityManager.close();
		}
	}
	


}
